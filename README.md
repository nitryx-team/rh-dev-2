[TOC]

# APLICAÇÃO DE 72H PARA O CARGO DE DESENVOLVEDOR FULLSTACK #

Olá querido candidato. Siga as instruções abaixo que fornecem todas as informações para o seu processo de contratação. Boa sorte !

## **1 -  Para que serve esse teste ?** ##

Este teste visa procurar um desenvolvedor para se juntar a nós com algumas habilidades técnicas / pessoais, como:
 
*  Ser capaz de procurar soluções por conta própria utilizando ferramentas como Google e StackOverflow       
*  Trabalhar com as tecnologias da empresa (Java, [Spring Boot](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/), [Angular](https://angular.io/), git ...)  
*  Pensamento orientado por *qualidade* e *performance* 
*  Mostrar as boas práticas de orientação a objetos e design de código na prática 
*  Seguir processos  

## **2 - Como faço para começar?** ##

*  1 - Envie um email com seu **nome completo** e seu **telefone** para **franklin.azeredo@nitryx.com
** com o título: **"Eu quero fazer o teste DEV-2"**
*  2 - Enquanto aguarda pela resposta com o esboço do projeto, configure seu ambiente com **Java SDK 11.x**, [LTS **Node.js** version](https://nodejs.org/en/download/), **[Angular](https://angular.io/guide/setup-local)**   
*  3 - Crie uma conta no [BitBucket](https://bitbucket.org/) ou [GitHub](https://github.com/) caso você não tenha uma. É gratuito e você vai precisar para nos enviar o teste.
*  4 - Recomendamos o uso do [IntelliJ IDEA](https://www.jetbrains.com/pt-br/idea) para desenvolver o backend e o [Visual Studio Code](https://code.visualstudio.com/download) para desenvolver o Frontend
*  5 - Quando receber o e-mail de confirmação, faça o download do arquivo .zip e descompacte ele em seu repositório. 
 
  
Você irá encontrar essa estrutura:


```
#!java

/backend  
/frontend  
```

Temos dois projetos: Um em **Spring Boot** com **Groovy** e o outro em **Angular** com **Typescript**. O primeiro é o nosso **backend** e o outro o nosso **frontend**


Abra o projeto Spring Boot (/backend) com a IDE, procure a classe BackendApplication.groovy e rode a aplicação para confirmar se está tudo ok.
 
Abra o projeto Angular (/frontend), com a IDE, abra um terminal dentro do diretório e rode:

```
#!java

npm install
```
Para finalizar o setup execute:

```
#!java

ng serve
```

Acesse [http://localhost:4200](http://localhost:4200) para ver o projeto rodando.


## **3 - Instruções** ##

Esse teste contém **7 tarefas** obrigatórias e **2 tarefas** opcionais descritas abaixo. Crie um **repositório publico** no seu Bitbucket(ou GitHub) com o nome **<SEU_NOME>_TEST_DEV_2**, Faça um **push** do seu trabalho para a branch **master**, e envie um e-mail para **franklin.azeredo@nitryx.com** com:

*  O título: **"Envio do teste DEV-2"**
*  Um link para seu projeto no Bitbucket (ou GitHub)

** OBS: Cuidado para não subir arquivos e pastas do Intellij, VS Code e a node_modules. Faça uso do .gitignore **

### **3.1 - Tarefa 1** ###

Boas novas! A primeira tarefa é apenas a Configuração e Setup do seu ambiente. Então, Provavelmente você já deve estar quase terminando nesse momento. Parabéns! Vamos para a próxima tarefa.

### **3.2 - Tarefa 2** ###

Essa tarefa será feita apenas no Backend:

**Crie** classes de domínio que representam:  

*  Um **Cliente** (com **Cpf** e **Nome**).
*  Uma **Transação** (com **Data e Hora** e **Valor**).
*  Um **Conta Bancária** (com **Número da agência**, **Número da conta**, **Saldo**, **Cliente** e uma coleção de **Transações**). 

No projeto há um **Runner** que executa logo após o container do Spring Boot levantar (com.nitryx.backend.application.BootstrapRunner.groovy). Dentro dele há um método run, vamos mockar alguns dados? Essa classe está anotada com um **@Component**, sendo um **Bean gerenciável do Spring**, em outras palavras, você pode **injetar outros componentes** dentro dela como **Serviços** e **Repositórios**.

*  Duas Contas correntes com seus respectivos clientes
*  Inicialize as Contas com saldo 0
*  Inicialize as Contas com transações vazias

O projeto **Spring Boot** já está configurado para usar um banco de dados em memória chamado **H2**. Olha que legal, podemos utilizar a poderosa API do **Spring Data** para persistir essas entidades que você criou.

Lembrando que o projeto é na linguagem **Groovy**, tenha atenção ao criar as classes.
A sintaxe do Java é compatível com a linguagem, mas caso queira se aprofundar na forma Groovy de codificar, siga a [documenteção](http://docs.groovy-lang.org/next/html/documentation/) da linguagem.

### **3.3 - Tarefa 3** ###

Até aqui está muito fácil não é? Ainda nesse Runner, vamos colocar **comportamento** em nosso código. 

* Credite um valor a primeira conta corrente
* Credite um valor a segunda conta corrente
* Debite um valor da segunda conta corrente

Mostre que você é um bom desenvolvedor orientado a objetos e que seu código é limpo! Use todas as boas práticas que você conhece, pense orientado no design de código, na legibilidade e na escalabilidade. 

** Pratique programação defensiva e valide as fronteiras em todas as camadas, seja qual for a arquitetura que você escolher para a implementação. **

Temos também uma regra de negócio:

**RN001 - Ao debitar, validar para que a Conta bancária nunca possa ter saldo negativo**

### **3.4 - Tarefa 4** ###

Chegou a hora de implementar a funcionalidade da transferência!

Implemente um serviço que recebe os **IDs** de duas **Contas**, a conta que envia o valor e a conta que recebe valor. O serviço precisa receber também o valor que vai ser transferido.

Ao final dessa execução, deve acontecer uma **Transferência**, ou seja, o **valor** ser **debitado** da **Conta corrente de origem** e ser **creditado** na **Conta corrente de destino**.

Execute esse service no Runner, logo após os Mocks do passo 3.3

### **3.5 - Tarefa 5** ###

Como sabemos se tudo está ocorrendo da maneira que a gente espera? 

Repare que no topo da classe BootstrapRunner.groovy, há uma anotação **@Slf4j**. Essa é uma forma elegante do Groovy disponibilizar para você uma instância da classe **Logger** da biblioteca [**SL4J**](http://slf4j.org/manual.html).

Que tal escrever um **log.info** para cada mudança de estado de nossos mocks, incluindo a execução do serviço?

### **3.6 - Tarefa 6 ** ###

O Log ajudou bastante não é?

Mas tem uma outra maneira mais elegante para verificarmos a consistência e estado dos nossos objetos, os testes automatizados.
No projeto há o **JUnit**, o **Mockito** e o **Spring Test** (Que permite testar utilizando o container de injeção de dependência). 

Crie **Testes unitários** para as ações de **Debitar em uma Conta**, **Creditar em uma Conta** e também para o **Serviço de transferência**. 

### **3.7 - Tarefa 7** ###

Chegou a hora da gente liberar uns endpoints para o nosso client!

O Spring Data possui uma API maravilhosa chamada: **JdbcTemplate**

Ela permite fazer queries em JDBC com facilidade, em alguns projetos precisamos descer o nível e fazer queries nativas por motivos de performance. Vamos ver como anda seu conhecimento em banco de dados?

Utilize o JdbcTemplate para isso. 

* Crie um endpoint do tipo **GET** com o caminho **"/contas"** e retorne todas as contas com a seguinte projeção: Id da conta, Nome do cliente, Número da agência, Número da conta e Saldo.

* Crie um endpoint do tipo **GET** com o caminho **"/contas/{idDaConta}/transacoes"** e retorne a coleção de transações associadas a uma conta corrente, ordenada por data e hora com a seguinte projeção: Id da conta, Data e hora da transação, Valor da Transação.

Use o navegador ou ferramentas como o **Postman** para verificar os resultados. 

### **3.8 - Tarefa 8 ** ###

Existe uma biblioteca chamada **Apache POI** que facilita a **leitura** e **escrita** de arquivos **.xls**.

Que tal gerar uma **Massa de Clientes e Conta Corrente** em uma planilha e **importar os dados** via .xls dentro do nosso Runner?

Vá em frente! Adicione o **Apache POI** via **Maven** pelo **pom.xml** e importe essa massa de dados de uma maneira mais otimizada. 

**Atenção: Separar em camadas o código de infraestrutura (I/O) do código de domínio (Regras de negócio)** 

### **3.9 - Tarefa 9** ###

Chegou a hora de consultar esses endpoints no Frontend! 

Queremos que você coloque em prática seus conhecimentos sobre criação de **Modelos em TypeScript**, os fabulosos **Serviços do Angular**, os **Componentes** com seus **Databinds** e a navegação por **Rotas**! 

**Não perca tempo com design e estilo nessa tarefa, foque em entregar valor primeiro.** 

* Na primeira página exiba em uma tabela a projeção com **Todas as Contas bancárias**
* Ao clicar no **Link do Nome do Cliente**, navegue para uma página que exibe todas as **Transações da Conta do Cliente**

Temos outra Regra de Negócio aqui:

**RN002 - Ao exibir todas as transações da Conta Corrente selecionada, a tabela deve ter uma coluna que mostra o saldo anterior a transação.**

Exemplo: 

**Lista de Contas Bancárias**

![Lista de Contas Bancárias](https://bitbucket.org/nitryx-team/rh-dev-2/raw/fe50f17a17ab8ec4b2a11ad79481c4a1ad66065f/1.png)

**Lista de Transações por Conta Bancária**

![Lista de Transações por Conta Bancária](https://bitbucket.org/nitryx-team/rh-dev-2/raw/fe50f17a17ab8ec4b2a11ad79481c4a1ad66065f/2.png)


## **Done!** ##
Muito obrigado pelo seu tempo !
**Não se esqueça**, faça um **push** do seu trabalho para a **Branch MASTER** e envie **um e-mail com o link do seu repositório** quando terminar!

Boa sorte!